<?php

namespace App\Http\Controllers;

use App\Talent;
use Illuminate\Http\Request;

class TalentController extends Controller
{
    public function getJson($category_id)
    {
        return Talent::where('talent_category_id',$category_id)->get();
    }
}
