<?php

namespace App\Http\Controllers;

use App\FormCampaign;
use App\Models\TalentCategory;
use App\Studio;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        $data['talent_categories'] = TalentCategory::get();
        $data['studios'] = Studio::get();
        return view('front.form',$data);
    }

    public function store(Request $request)
    {
        $form_campaign = new FormCampaign();
        $form_campaign->talent_category = $request->talent_category;
        $form_campaign->studio_name = $request->studio_name;
        $form_campaign->video_project_category = $request->video_project_category;
        $form_campaign->video_post = $request->video_post;
        $form_campaign->campaign_duration = $request->campaign_duration;
        $form_campaign->first_talent = $request->first_talent;
        $form_campaign->spare_talent = $request->spare_talent;
        $form_campaign->name = $request->name;
        $form_campaign->company = $request->company;
        $form_campaign->mobile = $request->mobile;
        $form_campaign->email = $request->email;
        $form_campaign->notes = $request->notes;
        $form_campaign->save();
        return redirect()->back()->with('success','Form submitted successfully');
    }
}
