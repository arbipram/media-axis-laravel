@extends('front.layouts.app')
@section('css')
    <style>
        h3{
            font-weight: bold;
        }

        .label-info, .badge-info{
            background-color: #20c997;
            border-radius: 5px;
            padding:2px;
        }

        .bootstrap-tagsinput {
            width: 30% !important;
        }
    </style>
@endsection
@section('content')
    <div id="app">
        <div class="container mb-5" style="background-color: white;">
            <div class="row">
                <div class="col-md-12 mt-4">
                    @foreach ($errors->all() as $error)
                        <label class="btn btn-danger"> {{ $error }} </label>
                    @endforeach
                    @if(session('success'))
                        <label class="btn btn-success btn-block"><i class="fa fa-check"></i>  {{ session('success') }} </label>
                    @endif
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="#" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group mt-4">
                                    <h3>1. Choose Your Talent</h3>
                                </div>
                            </div>
                            <div class="form-group mt-4 col-8">
                                <select class="form-control" name="talent_category" id="talent_category" @change="searchTalentCategory($event)">
                                    <option value="" disabled selected>--Select Talent Category--</option>
                                    @foreach ($talent_categories as $talent_category)
                                        <option value="{{ $talent_category->id }}">
                                            {{ $talent_category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-3" v-for="talent in this.talents">
                                <b class="mb-4"> @{{talent.name}}</b> <br>
                                <span v-html="talent.video_url"></span>
                                <label class="btn btn-sm btn-block btn-primary" @click="talentSelected(talent.name)"><i class="fa fa-user-plus"></i> Pick Talent</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label><b>* Please choose more than 1 talent for spare</b> </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <div class="form-group mt-4">
                                    <h3>2. Choose Your Studio</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($studios as $studio)
                                <div class="col-2">
                                    <div class="form-group">
                                        <img src="{{ $studio->images }}" style="width:150px;height:120px">
                                    </div>
                                    <div class="form-group" style="margin-left: 20px;">
                                        <input type="radio" name="studio_name" id="studio_name" class="form-check-input" value="{{ $studio->name }}">
                                        <label class="form-check-label"><b>{{ $studio->name }}</b></label>
                                    </div>
                                </div>
                            @endforeach
                            <br>
                            <div class="col-12">
                                <label>
                                    <b>* Your own location shooting will incur additional cost: Rp 2,500,000</b>
                                </label>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <h3>3. Choose Your Video Package Campaign</h3>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" value="media_social" name="video_project_category" id="media_social_selection">
                                    <label class="form-check-label">
                                        Media Social : 45 secs Video for Instagram, TikTok, FB, Youtube
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row media_social_show">
                            <div class="col-5">
                                <div class="form-group mt-4">
                                    <label>How many video post would you like per week ?</label>
                                </div>
                            </div>
                            <div class="form-group mt-4 col-7">
                                <select class="form-control" name="video_post" id="video_post">
                                    <option value="" disabled selected>--Video Post--</option>
                                    <option value="1 x per week">1 x per week</option>
                                    <option value="3 x per week">3 x per week</option>
                                    <option value="5 x per week">5 x per week</option>
                                    <option value="Everyday">Everyday</option>
                                </select>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label>How many month would you like to campaign ?</label>
                                </div>
                            </div>
                            <div class="form-group col-7">
                                <select class="form-control" name="campaign_duration" id="campaign_duration">
                                    <option value="" disabled selected>--Select Duration--</option>
                                    <option value="1 Month">1 Month </option>
                                    <option value="3 Month">3 Month </option>
                                    <option value="6 Month">6 Month </option>
                                    <option value="9 Month">9 Month </option>
                                    <option value="12 Month">12 Month </option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" value="long_duration" name="video_project_category" id="long_duration_selection">
                                    <label class="form-check-label">
                                        Long Duration Video for Company Profile / Tutorial / Product Review
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-6 mt-2 long_duration_show">
                                <select class="form-control" name="video_duration" id="video_duration">
                                    <option value="" disabled selected>--Select Duration--</option>
                                    <option value="Up to 15 mins">Up to 15 mins</option>
                                    <option value="Up to 45 mins">Up to 45 mins</option>
                                    <option value="Up to 60 mins">Up to 60 mins</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="form-group col-12">
                                <h3>4. Summary</h3>
                            </div>
                            <div class="form-group col-12 mt-4">
                                <div class="row">
                                    <div class="col-2">
                                        Your Talent is 
                                    </div>
                                    <div class="col-10">
                                        <input type="text" data-role="tagsinput" id="first_talent" name="first_talent" style="width: 300px" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-2">
                                        Your Spare Talent is
                                    </div>
                                    <div class="col-10">
                                        <input type="text" data-role="tagsinput" id="spare_talent" name="spare_talent" style="width: 250px"><br>
                                    </div>
                                </div>
                                * Minimum of 4 Videos per one talent <br>
                                <div class="media_social_show">
                                    You Choose <span id="video_post_selection" style="font-weight:bold"></span> posting for <span id="campaign_duration_selection" style="font-weight:bold"></span> campaign
                                </div>
                                <div class="long_duration_show">
                                    You Choose <span id="video_duration_selection" style="font-weight:bold"> </span> video <br>
                                </div>
                                Price is : Rp_________
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <div class="form-group">
                                    <h3>5. To Continue please register your Name and Contact</h3>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control col-md-6" placeholder="Enter Your Name"
                                        name="name" required>
                                </div>
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control col-md-6"
                                        placeholder="Enter Your Company Name" name="company" required>
                                </div>
                                <div class="form-group">
                                    <label>Mobile </label>
                                    <input type="text" class="form-control col-md-6"
                                        placeholder="Enter Your Phone Number" name="mobile" required>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control col-md-6"
                                        placeholder="Enter Your Email" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea name="notes" class="form-control col-md-6" cols="30" rows="3" required></textarea>
                                </div>
                            </div>
                        </div>
                    <button class="btn btn-primary col-6"><i class="fa fa-paper-plane"></i> Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Include optional progressbar HTML -->
@stop
@section('scripts')
<script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    const { createApp } = Vue

    createApp({
        data(){
            return{
                //berisi inisiasi data dan jenis data
                talents:[],
                first_talent : '',
                spare_talent : [],
                event : '',
            }
        },
        mounted(){
            //function yang diload ketika pertama kali halaman dibuka
            this.choosePackageCampaign();
            this.summary();
        },
        methods:{
            choosePackageCampaign(){
                $(".media_social_show").hide();
                $(".long_duration_show").hide();

                $("#media_social_selection").click(function(){
                    $(".media_social_show").show();
                    $(".long_duration_show").hide();
                });

                $("#long_duration_selection").click(function(){
                    $(".media_social_show").hide();
                    $(".long_duration_show").show();
                });
            },
            summary(){
                $("#video_post").change(function(){
                    var video_post = $("#video_post").val();
                    $("#video_post_selection").html(video_post);
                });
                
                $("#campaign_duration").change(function(){
                    var campaign_duration = $("#campaign_duration").val();
                    $("#campaign_duration_selection").html(campaign_duration);
                });

                $("#video_duration").change(function(){
                    var video_duration = $("#video_duration").val();
                    $("#video_duration_selection").html(video_duration);
                });
            },
            searchTalentCategory(event){
                axios.get("{{ url('talents') }}/"+event.target.value+".json").then(response => {
                    this.talents = response.data        
                }).catch(error => {
                    console.log(error)
                })
            },
            talentSelected(name){
                if(this.first_talent.length == 0){
                    this.first_talent = name;
                    $("#first_talent").tagsinput('add',name);
                } else {
                    $("#spare_talent").tagsinput('add',name);
                }
                alert("Talent Selected");
            }
        }
    }).mount('#app')
</script>
@stop
