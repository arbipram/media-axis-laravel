<!doctype html>
<html lang="en">
    @include('front.partials.head')
    @yield('css')
    <style>
    header{
        background-image: url("{{asset('/images/header.jpg')}}");
        height: 250px;
    }
    body{
        background-color:white;
    }
    </style>
    <body>
        @yield('content')
        @include('front.partials.scripts')
        @yield('scripts')
    </body>
</html>